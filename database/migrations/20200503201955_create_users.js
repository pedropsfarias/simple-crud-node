
exports.up = function(knex) {

    return knex.schema.createTable('users', function(table){

        table.increments('id');
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('cpf', 11).notNullable();
        table.date('birth').notNullable();
        table.string('hash').notNullable();

    });
  
};

exports.down = function(knex) {

    return knex.schema.droptable('users');
  
};
