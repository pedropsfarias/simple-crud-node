
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {
          name: 'Root',
          email: 'root@app',
          cpf: '11111111111',
          birth: '2000-01-01',
          hash: '40bd001563085fc35165329ea1ff5c5ecbdbbeef'
        }
      ]);
    });
};
