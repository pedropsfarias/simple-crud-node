var express = require('express');
var jwt = require('jsonwebtoken');
var database = require('../../api/middlewares/database.js');

var router = express.Router();

router.post("/", function (req, res) {

    const { email, hash } = req.body;

    if (email && hash) {

        database('users')
            .select('name', 'cpf', 'birth')
            .where({
                'email': email,
                'hash': hash
            })
            .then((users) => {

                const user = users[0];

                if (user) {

                    jwt.sign({ name: user.name, cpf: user.cpf }, "M1nh@ s3nha p2r2 crypt0grf@R", { expiresIn: '1d' }, (err, token) => {

                        if (err) {

                            res.json({ 
                                'success': false, 
                                'message': err.message || err 
                            });

                            return;
                        }

                        res.json({ 
                            'success': true, 
                            'data': { 
                                'token': token, 
                                'name': user.name, 
                                'cpf': user.cpf, 
                                'birth': user.birth 
                            } 
                        });

                        return;

                    });

                } else {

                    res.json({ 
                        'success': false, 
                        'message': 'Usuário ou senha incorretos.' 
                    });
                    
                    return;

                }

            });

    } else {

        res.json({ 
            'success': false, 
            'message': 'Parâmetros incorretos.' 
        });

        return;

    }

});

module.exports = router;