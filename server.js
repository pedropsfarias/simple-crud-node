var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var router = express.Router();
var app = express();

var validateToken = require('./api/middlewares/validateToken.js');
var auth = require('./api/routes/auth.js');
var users = require('./api/routes/users.js');

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('./public'));

// Rotas públicas
router.use('/auth', auth);

// Rotas privadas
router.use(validateToken);
router.use('/users', users);

app.use('/api', router);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});