var jwt = require('jsonwebtoken');

var validateToken = function (req, res, next) {

    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

    if (token) {

        jwt.verify(token, "M1nh@ s3nha p2r2 crypt0grf@R", function (err, decoded) {
            if (err) {

                if(err.message == 'jwt expired'){
                    return res.json({success: false, message: "Sessão expirada.", invalidToken: true});
                } else {
                    return res.json({success: false, message: err.message || err, invalidToken: true });
                }
            } else {
                req.emailUsuario = decoded.email;
                req.nomeUsuario = decoded.nome;
                next();
            }
        });

    } else {
        return res.json({ success: false, message: "Sessão expirada.", invalidToken: true });
    }
};

module.exports = validateToken;